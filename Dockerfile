# Стадия сборки
FROM golang:1.19 as builder

# Рабочая директория внутри контейнера
WORKDIR /daemon

# Копируем файлы Go модуля и его зависимости
COPY go.mod go.sum ./
RUN go mod download

# Копируем исходный код приложения в контейнер
COPY . .

# Компилируем приложение
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o moderation-daemon .

# Стадия создания финального образа
FROM debian:latest

# Обновляем список пакетов и устанавливаем корневые сертификаты
RUN apt-get update && \
    apt-get install -y ca-certificates && \
    update-ca-certificates

# Копируем исполняемый файл из стадии сборки
COPY --from=builder /daemon/moderation-daemon /usr/local/bin/moderation-daemon

# Копируем forbidden_words.csv в рабочую директорию /daemon
COPY --from=builder /daemon/forbidden_words.csv /usr/local/bin/forbidden_words.csv

# Назначаем исполняемый файл как точку входа
ENTRYPOINT ["/usr/local/bin/moderation-daemon"]
