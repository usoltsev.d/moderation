package main

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"log"
	"moderation/pkg/kafka"
	"moderation/pkg/moderation"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	log.Println("Фоновая программа Moderation запущена")

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Настройки для Kafka
	broker := os.Getenv("KAFKA_BROKER")
	if broker == "" {
		log.Fatal("Переменная окружения KAFKA_BROKER не задана")
	}

	groupID := os.Getenv("KAFKA_GROUP_ID")
	if broker == "" {
		log.Fatal("Переменная окружения KAFKA_GROUP_ID не задана")
	}

	inputTopic := os.Getenv("KAFKA_INPUT_TOPIC")
	if broker == "" {
		log.Fatal("Переменная окружения KAFKA_INPUT_TOPIC не задана")
	}

	outputTopic := os.Getenv("KAFKA_OUTPUT_TOPIC")
	if broker == "" {
		log.Fatal("Переменная окружения KAFKA_OUTPUT_TOPIC не задана")
	}

	// Инициализация Kafka Producer и Consumer
	producer := kafka.NewProducer([]string{broker})
	consumer := kafka.NewConsumer([]string{broker}, groupID, inputTopic)

	// Инициализация структуры для хранения недопустимых слов и загрузка слов из csv
	forbiddenWords := moderation.NewForbiddenWords()

	// Загрузка
	filePath := "/usr/local/bin/forbidden_words.csv"
	data, err := ReadCSV(filePath)
	if err != nil {
		panic(err)
	}

	forbiddenWords.LoadForbiddenWords(data)

	log.Println("Список недопустипых слов:", forbiddenWords.Words)

	// Обработка системных сигналов
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	// Основной цикл обработки
	go func() {
		for {
			select {
			case <-ctx.Done():
				// Контекст был отменен, выходим из горутины
				return
			default:

				// Чтение сообщения из Kafka
				msg, err := consumer.Read()
				if err != nil {
					log.Printf("Ошибка при чтении сообщения из Kafka: %v", err)
					continue
				}

				var comment moderation.Comment

				// Десериализация JSON в структуру Comment
				err = json.Unmarshal(msg.Value, &comment)
				if err != nil {
					log.Printf("Ошибка при десериализации JSON в структуру Comment: %v", err)
					continue
				}

				// Модерация комментария
				forbiddenWords.ModerateComment(ctx, &comment)

				// Cериализация структуры Comment в JSON
				ModeratedComment, err := json.Marshal(comment)
				if err != nil {
					log.Printf("Ошибка при сериализации структуры Comment в JSON: %v", err)
					continue
				}

				// Отправка сообщения в Kafka
				err = producer.Send(outputTopic, ModeratedComment)
				if err != nil {
					log.Printf("Ошибка при отправке сообщения в Kafka: %v", err)
				}
			}

		}
	}()

	// Ожидание сигнала для завершения
	<-signals

	log.Println("Фоновая программа Moderation успешно остановлена")
}

// ReadCSV возвращает данные из CSV файла в виде среза срезов строк.
func ReadCSV(filePath string) ([][]string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	reader := csv.NewReader(file)
	records, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}

	return records, nil
}
