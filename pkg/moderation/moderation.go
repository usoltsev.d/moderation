package moderation

import (
	"context"
	"strings"
)

// Comment структура необходимая для модерации комментария
type Comment struct {
	ID            int    `json:"id"`
	CommentText   string `json:"comment_text"`
	CommentStatus int    `json:"comment_status"` // 0 - На модерации, 1 - Опубликован, 2 - Заблокирован
}

// ForbiddenWords структура для хранения запрещенных слов
type ForbiddenWords struct {
	Words map[string]struct{}
}

func NewForbiddenWords() *ForbiddenWords {
	return &ForbiddenWords{Words: make(map[string]struct{})}
}

// LoadForbiddenWords загрузка недопустипых слов
func (f *ForbiddenWords) LoadForbiddenWords(words [][]string) {
	for _, record := range words {
		for _, word := range record {
			f.Words[word] = struct{}{}
		}
	}
}

// ModerateComment проверяет комментарий на наличие запрещенных слов и обновляет его статус.
func (fw *ForbiddenWords) ModerateComment(ctx context.Context, comment *Comment) {
	for _, word := range strings.Fields(strings.ToLower(comment.CommentText)) {
		if _, exists := fw.Words[word]; exists {
			comment.CommentStatus = 3 // Заблокирован
			return
		}
	}
	comment.CommentStatus = 2 // Опубликован
}
