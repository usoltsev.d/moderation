package kafka

import (
	"context"

	"github.com/segmentio/kafka-go"
)

// Producer структура для Kafka Producer
type Producer struct {
	writer *kafka.Writer
}

// NewProducer создает нового Kafka Producer
func NewProducer(brokers []string) *Producer {
	writer := kafka.NewWriter(kafka.WriterConfig{
		Brokers:  brokers,
		Balancer: &kafka.LeastBytes{},
	})
	return &Producer{writer: writer}
}

// Send отправляет сообщение в Kafka
func (p *Producer) Send(topic string, message []byte) error {
	msg := kafka.Message{
		Topic: topic,
		Value: message,
	}
	return p.writer.WriteMessages(context.Background(), msg)
}

// Consumer структура для Kafka Consumer
type Consumer struct {
	reader *kafka.Reader
}

// NewConsumer создает нового Kafka Consumer
func NewConsumer(brokers []string, groupID string, topic string) *Consumer {
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  brokers,
		GroupID:  groupID,
		Topic:    topic,
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})
	return &Consumer{reader: reader}
}

// Read читает сообщения из Kafka
func (c *Consumer) Read() (kafka.Message, error) {
	return c.reader.ReadMessage(context.Background())
}

// Close закрывает Consumer и освобождает ресурсы
func (c *Consumer) Close() error {
	return c.reader.Close()
}
